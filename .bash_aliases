#hosts
HOST=pi

#ssh
alias scomeflores='ssh comeflores@comeflores.net -p 3222'
alias binario='torify ssh binario@elbinario.net -p 3222'
alias spi='ssh haton@hatpi.nsupdate.info'

#ssh local
alias slpi='ssh haton@pi'
alias slfixo='ssh user@fixo'
alias slcubie='ssh haton@cubie'

#short commands
alias ls='ls --color'
alias em='emacs24 -nw'
alias ecron="EDITOR=nano crontab -e"
alias cfp='cd ~/FP'

#root commands
alias poweroff='su -c "poweroff"'
alias reboot='su -c "reboot"'

#mount points
alias mntcubie='sshfs haton@hatpi.nsupdate.info:/mnt/ ~/cubie/'

#random
alias captura="scrot -d 5 'compartir.png' && pomfload compartir.png && rm compartir.png"
alias sharefolder='python2.7 -m SimpleHTTPServer 8888'
alias getip='wget -O - -q icanhazip.com'

#todo
alias fpsync='rsync -rtv ~/FP/ haton@$HOST:~/hd/FP/'
alias todo='todotxt-machine ~/.todo/todo.txt && rsync -rtv ~/.todo/ haton@$HOST:~/hd/todo'

#git
alias gadd='git add'
alias gcommit='git commit -m'
alias gpull='git pull'
alias gpush='git push origin master'
alias gclone='git clone'
alias gstatus='git status'
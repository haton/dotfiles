# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt notify
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/user/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#PS1='%n@%M:%~ '
autoload -U colors && colors
PROMPT="%{$fg[green]%}%n@%{$fg[green]%}%m %{$fg_no_bold[yellow]%}%1~ %{$reset_color%}"
RPROMPT="[%{$fg_no_bold[yellow]%}%?%{$reset_color%}]"
export PATH=/home/user/.scripts/:$PATH
alias ls='ls --color'


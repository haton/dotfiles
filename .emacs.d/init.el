;; Paths
(add-to-list 'load-path "~/.emacs.d/elpa/")

;; Install From Melpa
(require 'package) ;; You might already have this line
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line


;; Codification
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)) 

;; Theme
(load-theme 'monokai t)

;; Keymaps
(global-set-key (kbd "C-a") 'auto-complete-mode)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key [f12] 'eshell)

;; Custom commands
(defun gtd ()
   (interactive)
   (find-file "~/todo.org")
 )

;; Lines Numeration
(add-hook 'find-file-hook (lambda () (linum-mode 1)))
(global-linum-mode )
(setq linum-format "%4d \u2502 ")

;; Auto-indent
(global-set-key (kbd "RET") 'newline-and-indent)

;; Autocomplete
(add-to-list 'load-path "~/.emacs.d/ac")
(require 'auto-complete-config)
(ac-config-default)

;; Compile comand
(setq compile-command "gcc -U_FORTIFY_SOURCE -Wall -Wuninitialized -Werror -O1") 

;; Modes
; Markdown Mode
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

;; ibuffer
;; no mostrar buffers que empiezan por *
(defun next-code-buffer ()
  (interactive)
  (let (( bread-crumb (buffer-name) ))
    (next-buffer)
    (while
        (and
         (string-match-p "^\*" (buffer-name))
         (not ( equal bread-crumb (buffer-name) )) )
      (next-buffer))))
(global-set-key [remap next-buffer] 'next-code-buffer)

;; groups
(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("dir" (mode . dired-mode))
	       ;;("perl" (mode . cperl-mode))
	       ;;("erc" (mode . erc-mode))
	       ;; ("planner" (or
	       ;;             (name . "^\\*Calendar\\*$")
	       ;;             (name . "^diary$")
	       ;;             (mode . muse-mode)))
	       ("web" (or
		       (mode . web-mode)
		       (mode . php-mode)
		       (mode . html-mode)
		       (mode . css-mode)))
	       ("sql" (mode . sql-mode))
	       ("python" (mode . python-mode))
	       ("lisp" (mode . emacs-lisp-mode))
	       ("emacs" (or
			 (name . "^\\*scratch\\*$")
			 (name . "^\\*Messages\\*$")
			 (name . "^\\*GNU Emacs\\*$")
			 (name . "^\\*Help\\*$")
			 (name . "^\\*Completions\\*$")))
	       ("gnus" (or
			(mode . message-mode)
			(mode . bbdb-mode)
			(mode . mail-mode)
			(mode . gnus-group-mode)
			(mode . gnus-summary-mode)
			(mode . gnus-article-mode)
			(name . "^\\.bbdb$")
			(name . "^\\.newsrc-dribble")))
	       
	       ))))

(add-hook 'ibuffer-mode-hook
	  (lambda ()
	    (ibuffer-switch-to-saved-filter-groups "default")))

;; Miscellaneous
; Nyan mode
(nyan-mode)

; Desactivar la tool-bar
(tool-bar-mode -1)

; Desactivar scroll-bar
(scroll-bar-mode -1)

; Desactivar menu-bar
;(menu-bar-mode -1)

; Desactivar guardado automático
(setq auto-save-default nil) 
(custom-set-variables
 (put 'upcase-region 'disabled nil)
 '(initial-buffer-choice "~/todo.org")
 )

;; don't let the cursor go into minibuffer prompt
(setq minibuffer-prompt-properties (quote (read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)))


;; hexadecimal color on css and html
(defun xah-syntax-color-hex ()
"Syntax color hex color spec such as 「#ff1100」 in current buffer."
  (interactive)
  (font-lock-add-keywords
   nil
   '(("#[abcdef[:digit:]]\\{6\\}"
      (0 (put-text-property
          (match-beginning 0)
          (match-end 0)
          'face (list :background (match-string-no-properties 0)))))))
  (font-lock-fontify-buffer)
  )

(add-hook 'css-mode-hook 'xah-syntax-color-hex)
(add-hook 'html-mode-hook 'xah-syntax-color-hex)
